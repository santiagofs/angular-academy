<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Product extends Model 
{
    protected $fillable = [
        'name', 'description', 'img_url', 'price'
    ];

    protected $hidden = [
        'img_url', 'created_at'
    ];

    

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Hashing\BcryptHasher as Hash;

class User extends Model 
{
    public static $current = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token', 'role_id', 'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token_expiration', 'created_at', 'updated_at'
    ];

    public function setPasswordAttribute($value) {
	    if(!$value) return;
		$this->attributes['password'] = (new Hash)->make($value);
    }

    public static function auth($email, $password) {

	    $user = static::where('email', $email)->first();
	    if(!$user) return false;


	    if( !(new Hash)->check($password, $user->password) ) return false;

	    $user->api_token = str_random(60);
	    $user->api_token_expiration = static::token_expiration();
	    $user->save();

	    return $user;

    }

    public function keep_alive() {
	    $this->api_token_expiration = static::token_expiration();
	    $this->save();
    }

    protected static function token_expiration ()
    {
	    return date("Y-m-d H:i:s", strtotime('+1 hours'));
    }

    public function revalidate() {
	    $this->api_token_expiration = static::token_expiration();
	    $this->save();
    }

    public static function check($token) {
	    $mytime = \Carbon\Carbon::now();
	    $user = static::where('api_token', $token)
	    	->where('api_token_expiration', '>', $mytime->toDateTimeString())->first();
	    if($user) $user->revalidate();
	    static::$current = $user;
	    return $user;
    }

}
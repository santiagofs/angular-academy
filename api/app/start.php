<?php
ini_set('display_errors', 'On');

$loader = require realpath(__DIR__ .'/../vendor/autoload.php');
$database = require realpath(__DIR__ .'/database.php');

$loader->add('App', __DIR__);

use Illuminate\Database\Capsule\Manager as Capsule;
// Service factory for the ORM
$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);
    
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    return $capsule;
};


$settings = require '../app/settings.php';

$app = new \Slim\App($settings);
$app->db = function() {
    return new Capsule();
};

<?php

require realpath(__DIR__.'/models/user.php');  
require realpath(__DIR__.'/models/product.php');  

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \App\Models\User as User;


// $app->add(function ($req, $res, $next) {
//     $response = $next($req, $res);
//     return $response
//             ->withHeader('Access-Control-Allow-Origin', '*')
//             ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
//             ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
// });

// public static function unauthorized() {
//     return response()->json(['message'=>'You are not allowed on this resource'], 403);
// }

$app->get('/', function(Request $request, Response $response) {
    echo('index');
});

$app->get('/test', function(Request $request, Response $response) use($app) {
    //echo('test');
    //$parsedBody = $request->getParsedBody();
    $params = $request->getQueryParams();
    $email = $params['user'];
    $password = $params['password'];
    
    $user = User::where('email', $email)->first();

    if( !($user = User::auth($email, $password)) ) return $response->withJson('This resource needs authentication', 401);

    return $response->withJson($user);
});
$app->post('/test2', function(Request $request, Response $response) use($app) {
    return $response->withJson(['rsp' => 'test']);
});

$app->post('/login', function(Request $request, Response $response) use($app) {
    //echo('test');
    //$parsedBody = $request->getParsedBody();
    $params = $request->getParsedBody();
    $email = $params['username'];
    $password = $params['password'];
    
    $user = User::where('email', $email)->first();

    if( !($user = User::auth($email, $password)) ) return $response->withJson('Invalid user or password', 401);

    return $response->withJson($user);
});

// user routes
$app->get('/users/current', function(Request $request, Response $response) use($app) {
    $headers = $request->getHeaders();
    $token = @$headers['HTTP_X_AUTH_TOKEN'];

    if(!$token) return $response->withJson(null);

    $user = App\Models\User::check($token);

    return $response->withJson($user);
});
$app->get('/users', function(Request $request, Response $response) use($app) {

    $users = App\Models\User::get();

    return $response->withJson($users);
});

$app->post('/users', function(Request $request, Response $response) use($app) {

    $params = (object)$request->getParsedBody();
    $user = App\Models\User::find($params->id);
    $user || $user = new App\Models\User;

    $user->name = $params->name;
    $user->email = $params->email;
    if($params->password) $user->password = $params->password;
    $user->role_id = $params->role_id;
    $user->save();

    return $response->withJson($user);
});


$app->options('/users/{id}', function ($request, $response, $args) {
    return $response;
});
$app->delete('/users/{id}', function(Request $request, Response $response, $args) use($app) {
    $user = App\Models\User::find($args['id']);
    if($user) $user->delete();
    return $response->withJson($args['id']);
});



// product routes
$app->get('/products', function(Request $request, Response $response) use($app) {

    $products = App\Models\Product::get();

    return $response->withJson($products);
});

$app->post('/products', function(Request $request, Response $response) use($app) {

    $params = (object)$request->getParsedBody();
    $product = App\Models\Product::find($params->id);
    $product || $product = new App\Models\Product;

    $product->name = $params->name;
    $product->description = $params->description;
    $product->price = $params->price;
    $product->save();

    return $response->withJson($product);
});


$app->options('/products/{id}', function ($request, $response, $args) {
    return $response;
});
$app->delete('/products/{id}', function(Request $request, Response $response, $args) use($app) {
    
    $product = App\Models\Product::find($args['id']);
    if($product) $product->delete();
    return $response->withJson($args['id']);
});


// 	$token = app('request')->header('authorization');

// 	$token = app('request')->input('api_token', $token);
// 	if(!$token) return null;

// 	$user = App\Models\User::check($token);
// 	return $user;
// }
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AppService {

    private serviceUrl: string = 'http://ac-api.dev/';
    private token: string = '';

    constructor(private http: HttpClient){}

    private getAuthHeaders() {
        console.log(this.token);
        return new HttpHeaders().set('X-Auth-Token', this.token);
    }
    public test() {
        var url = this.serviceUrl+'test2';
        return this.http.post(url, {});
    }

    public authenticate(user:string, password: string) {
        var url = this.serviceUrl + 'login';

        var promise = new Promise((resolve, reject) => {
            this.http.post(url, {'username': user, 'password': password}).subscribe(data=>{
                console.log(data);
                this.token = data['api_token'];
                console.log(this.token);
                resolve(data);
            }, err => {
                console.log(err);
                reject(err);
            });
        });

        return promise;
    }

    public getCurrentUser() {
        var url = this.serviceUrl + 'users/current';
        var options = {headers: this.getAuthHeaders()};
        console.log(options);
        return this.http.get(url, options);        
    }
}
import { Component } from '@angular/core';
import { AppService } from './app.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private appService: AppService) {}
  
  authResponse: object = null;
  authError: string = '';
  authenticate(user:string, password: string):void{
    this.authResponse = null;
    this.authError = '';
    this.appService.authenticate(user, password).then(data => {
      this.authResponse = data;
    }, err => {
      this.authError = err.error;
    });
  }


  currentUserResponse: object = null;
  currentUser() {
    this.currentUserResponse = null;
    this.appService.getCurrentUser().subscribe(data=>{
      this.currentUserResponse = data;
    });
  }
}

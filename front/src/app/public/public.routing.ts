import { UserGuard } from './../store/shared/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicComponent } from './public.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';

import { UserComponent } from './user/user.component';
import { ProfileComponent } from './user/profile/profile.component';
import { OrdersComponent } from './user/orders/orders.component';
import { ErrorComponent } from './order/error/error.component';
import { SuccessComponent } from './order/success/success.component';
import { PaymentComponent } from './order/payment/payment.component';
import { BillingComponent } from './order/billing/billing.component';
import { ReviewComponent } from './order/review/review.component';
import { CartComponent } from './order/cart/cart.component';
import { OrderComponent } from './order/order.component';



const routes: Routes = [
  { path: 'order', component: OrderComponent , children: [
    { path: 'cart', component: CartComponent },
    { path: 'review', component: ReviewComponent, canActivate: [UserGuard] },
    { path: 'billing', component: BillingComponent, canActivate: [UserGuard] },
    { path: 'payment', component: PaymentComponent, canActivate: [UserGuard] },
    { path: 'success', component: SuccessComponent, canActivate: [UserGuard] },
    { path: 'error', component: ErrorComponent, canActivate: [UserGuard] },
  ]},
  { path: '', component: PublicComponent, children: [
    { path: '', component: ProductListComponent }, // product list
    { path: ':id', component: ProductDetailComponent } // product detail
  ]},
  { path: 'user', component: UserComponent , children: [
    { path: 'profile', component: ProfileComponent },
    { path: 'orders', component: OrdersComponent }
  ], canActivate: [UserGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRouting {}

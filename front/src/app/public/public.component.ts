import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';


import * as PublicActions from './store/public.actions';
import * as fromPublic from './store/public.reducers';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {

  constructor(
    private store: Store<fromPublic.State>
  ) {}
  
  ngOnInit() {
  }

}


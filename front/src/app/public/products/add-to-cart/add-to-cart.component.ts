import { Component, Input, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { Product } from '../../../store/products/product.model'

import * as PublicActions from '../../store/public.actions';
//import * as fromPublic from '../../store/public.reducers';
import * as fromCart from '../../store/cart.reducers';

import * as fromApp from '../../../app.reducers';



@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent {
  @Input() product: Product;
  @Input() withLabel: boolean = false;

  alreadyInCart: boolean = false;

  constructor(
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit() {
    this.store.select('cart').subscribe((cartState:fromCart.State) => {
      this.alreadyInCart = (cartState.cart.filter(e => {
        return e.product.id === this.product.id;
      }).length > 0);
    });
  }
  
  onClick() {
    this.store.dispatch(new PublicActions.CartAdd(this.product));
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromProducts from '../../../store/products/products.reducers';
import { Product } from '../../../store/products/product.model'

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  productsState: Observable<fromProducts.State>;
  product: Product;
  id: number;
  
  constructor(private router: Router,
      private route: ActivatedRoute,
      private store: Store<fromProducts.FeatureState>) {}

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.store.select('products').subscribe((productsState:fromProducts.State) => {
          let filtered = productsState.products.filter((e,i,a) => {
            return e.id === this.id;
          })
          if(filtered.length === 0) {
            this.router.navigate(['not-found']);
          }
          this.product = filtered[0];
        });
        
      }
    );
  }

}

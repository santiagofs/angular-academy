import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import * as fromShared from '../../../store/shared/shared.reducers';
import * as fromApp from '../../../app.reducers'

import { CartItem } from '../../store/cart-item.model';
import * as fromCart from '../../store/cart.reducers';

import * as PublicActions from '../../store/public.actions';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: CartItem[];
  cartTotal: number = 0;

  constructor(
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit() {
    this.store.select('cart').subscribe((cartState:fromCart.State) => {
      this.cart = cartState.cart;
      if(cartState.cart.length) {
        this.cartTotal = cartState.cart
              .map(e=>e.product.price * e.qty)
              .reduce((o, n)=>{
                return o+n;
              });
      };
    });

  }

  onQtyChange(id, event) {
    let qty = +event.target.value;
    if(qty===0) {
      this.store.dispatch(new PublicActions.CartRemove({id:id, all: true}));
    } else {
      this.store.dispatch(new PublicActions.CartQty({id:id, qty: qty}));
    }
    
  }

  onRemove(id) {
    this.store.dispatch(new PublicActions.CartRemove({id:id, all: true}));
  }

}

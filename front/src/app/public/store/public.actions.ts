import { Action } from '@ngrx/store';

import { Product } from '../../store/products/product.model';

export const CART_ADD = 'CART_ADD';
export const CART_QTY = 'CART_QTY';
export const CART_REMOVE = 'CART_REMOVE';
export const CART_CLEAR = 'CART_CLEAR';

export class CartAdd implements Action {
  readonly type = CART_ADD;
  constructor(public payload: Product) {}
}
export class CartQty implements Action {
  readonly type = CART_QTY;
  constructor(public payload: {id: number, qty:number}) {}
}

export class CartRemove implements Action {
  readonly type = CART_REMOVE;
  constructor(public payload: {id: number, all?:boolean}) {
      if(this.payload.all === undefined) this.payload.all = false;
  }
}
export class CartClear implements Action {
    readonly type = CART_CLEAR;
}

export type CartActions = CartAdd | CartRemove | CartClear | CartQty;





export type PublicActions = CartActions;
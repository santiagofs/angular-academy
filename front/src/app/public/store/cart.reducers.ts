import { Action } from '@ngrx/store';


import { Product } from '../../store/products/product.model';

import * as PublicActions from './public.actions';

import { CartItem } from './cart-item.model';

export interface State {
  cart: CartItem[],
}

const initialState: State = {
    cart: []
};

function cartItemById(cart, id) {
    let index = cart.findIndex((e,i,a)=>{
        return e.product.id === id;
    });
    return index;
}


export function CartReducers(state = initialState, action: PublicActions.CartActions) {
    
    switch (action.type) {

        case PublicActions.CART_ADD:
            let addIndex = cartItemById(state.cart, action.payload.id);
            let addCart:CartItem[];
            if(addIndex === -1) {
                addCart = [...state.cart, {product: action.payload, qty: 1}];
            }
            return {
                ...state,
                cart: addCart
            };

            
        case PublicActions.CART_QTY:
            let qtyIndex = cartItemById(state.cart, action.payload.id);
            if(qtyIndex === -1) return state;
            
            return {
                ...state,
                cart: state.cart.map((e,i,a)=>{ return (i === qtyIndex) ? {...e, qty: action.payload.qty} : e })
            };
        

        case PublicActions.CART_REMOVE:
            let removeIndex = cartItemById(state.cart, action.payload.id);
            if(removeIndex === -1) return state;

            let removeCart = [...state.cart];
            if(action.payload.all || state.cart[removeIndex].qty===1) {
                removeCart.splice(removeIndex,1);
            } else {
                state.cart.map((e,i,a)=>{ return (i === removeIndex) ? {...e, qty: e.qty-1} : e });
            }

            return {
                ...state,
                cart: removeCart
            };


        case PublicActions.CART_CLEAR:
            return {
                ...state,
                cart: []
            };


    default:
      return state;
  }
}

import { ActionReducerMap } from '@ngrx/store';

import * as fromCart from './cart.reducers';


export interface State {
  cart: fromCart.State
}

export const PublicReducers: ActionReducerMap<State> = {
  cart: fromCart.CartReducers
};

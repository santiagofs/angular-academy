import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { HttpClient, HttpRequest } from '@angular/common/http';

import { Store } from '@ngrx/store';

import * as UsersActions from './users.actions';
import { User } from './user.model';
import * as UsersReducers from './users.reducers';


@Injectable()
export class UsersEffects {

    @Effect()
    usersFetch = this.actions$
        .ofType(UsersActions.USERS_FETCH)
        .switchMap((action: UsersActions.UsersFetch) => {
            return this.httpClient.get<User[]>('http://ac-api.dev/users', {
            observe: 'body',
            responseType: 'json'
          })
        })
        .map(
          (users) => {
            console.log('users fetched')
            const localUsers = users.map(e => {
              return new User(e);
            });
            return {
              type: UsersActions.USERS_SET,
              payload: localUsers
            };
          }
        );
    
    @Effect()
    usersCreate = this.actions$
        .ofType(UsersActions.USERS_SAVE)
        .switchMap((action: UsersActions.UsersSave) => {
          console.log('trigger save')
          return this.httpClient.post('http://ac-api.dev/users', action.payload)
              .map( response => {
                  let actionType = action.payload.id === 0 ? UsersActions.USERS_ADD : UsersActions.USERS_UPDATE;
                  return {
                      type: actionType,
                      payload: new User(response)
                  };
              })
              // .catch(rsp => {
              //     return Observable.of({
              //         type: SharedActions.SHARED_LOGIN_ERROR,
              //     });
              // });
      })
    
  @Effect()
  usersDelete = this.actions$
      .ofType(UsersActions.USERS_DELETE)
      .switchMap((action: UsersActions.UsersDelete) => {
        let url = `http://ac-api.dev/users/${action.payload}`;
        return this.httpClient.delete(url)
            .map( response => {
                return {
                    type: UsersActions.USERS_ONDELETE,
                    payload: action.payload
                };
            })
            // .catch(rsp => {
            //     return Observable.of({
            //         type: SharedActions.SHARED_LOGIN_ERROR,
            //     });
            // });
    })

  constructor(private actions$: Actions,
              private httpClient: HttpClient,
              private store: Store<UsersReducers.State>) {}
}

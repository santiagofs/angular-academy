export class User {
    public id = 0;
    public name: string;
    public email: string;
    public api_token: string;
    public role_id: number;

    constructor(data?: any) {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.email = data.email;
            this.api_token = data.api_token;
            this.role_id = data.role_id;
        }
    }
// tslint:disable-next-line:eofline
}
import { User } from './user.model';

import * as UsersActions from './users.actions';


export interface State {
    users: User[];
    fetched: boolean;
}

const initialState: State = {
  users: [],
  fetched: false
};

function indexById(users: User[], id) {
  const index = users.findIndex((e, i, a) => {
      return e.id === id;
  });
  return index;
}

export function UsersReducers(state = initialState, action: UsersActions.UsersActions) {
  switch (action.type) {
    case UsersActions.USERS_SET:
    return {
      ...state,
      users: [...action.payload],
      fetched: true
    };

  case UsersActions.USERS_ADD:
    return {
      ...state,
      users: [...state.users, action.payload]
    };

  case UsersActions.USERS_UPDATE:
  const updateIndex = indexById(state.users, action.payload.id);
    if(updateIndex === -1) return state;
    const updateUsers = [...state.users];
    updateUsers[updateIndex] = action.payload;
    return {
      ...state,
      users: updateUsers
    };

  case UsersActions.USERS_ONDELETE:
    let deleteIndex = indexById(state.users, action.payload);
    let deleteUsers = [...state.users]
    deleteUsers.splice(deleteIndex, 1)
    return {
      ...state,
      users: deleteUsers
    };
    default:
      return state;
  }
}

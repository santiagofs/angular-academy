import { Product } from '../products/product.model';

export class Order {
  constructor(
      public id: number,
      public user_id: number,
      public total: number,
      public billing_address_id: number,
      public shipping_address_id: number,
      public products: Product[]
    ) {}
}

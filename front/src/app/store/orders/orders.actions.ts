
import { Action } from '@ngrx/store';

import { Order } from './order.model';

export const ORDERS_FETCH = 'ORDERS_FETCH';
export const ORDERS_SET = 'ORDERS_SET';
export const ORDERS_ADD = 'ORDERS_ADD';
export const ORDERS_UPDATE = 'ORDERS_UPDATE';
export const ORDERS_DELETE = 'PRODUCS_DELETE';


export class OrdersFetch implements Action {
  readonly type = ORDERS_FETCH;
  //constructor(public payload: Recipe[]) {}
}

export class OrdersSet implements Action {
  readonly type = ORDERS_SET;
  //constructor(public payload: Recipe) {}
}

export class OrdersAdd implements Action {
  readonly type = ORDERS_ADD;
  //constructor(public payload: {index: number, updatedRecipe: Recipe}) {}
}


export class OrdersUpdate implements Action {
readonly type = ORDERS_UPDATE;
}

export class OrdersDelete implements Action {
  readonly type = ORDERS_DELETE;
  constructor(public payload: number) {}
}

export type OrdersActions = OrdersFetch |
    OrdersSet |
    OrdersAdd |
    OrdersUpdate |
    OrdersDelete;
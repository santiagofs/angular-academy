import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { HttpClient, HttpRequest } from '@angular/common/http';

import { Store } from '@ngrx/store';

import * as ProductsActions from './products.actions';
import { Product } from './product.model';
import * as ProductsReducers from './products.reducers';


@Injectable()
export class ProductsEffects {

    @Effect()
    productsFetch = this.actions$
        .ofType(ProductsActions.PRODUCTS_FETCH)
        .switchMap((action: ProductsActions.ProductsFetch) => {
            return this.httpClient.get<Product[]>('http://ac-api.dev/products', {
            observe: 'body',
            responseType: 'json'
          })
        })
        .map(
          (products) => {
            var localProducts = products.map(e => {
              return new Product(e);
            })
            return {
              type: ProductsActions.PRODUCTS_SET,
              payload: localProducts
            };
          }
        );
    
    @Effect()
    productsCreate = this.actions$
        .ofType(ProductsActions.PRODUCTS_SAVE)
        .switchMap((action: ProductsActions.ProductsSave) => {
          console.log('trigger save')
          return this.httpClient.post('http://ac-api.dev/products', action.payload)
              .map( response => {
                  let actionType = action.payload.id === 0 ? ProductsActions.PRODUCTS_ADD : ProductsActions.PRODUCTS_UPDATE;
                  return {
                      type: actionType,
                      payload: new Product(response)
                  };
              })
              // .catch(rsp => {
              //     return Observable.of({
              //         type: SharedActions.SHARED_LOGIN_ERROR,
              //     });
              // });
      })
    
  @Effect()
  productsDelete = this.actions$
      .ofType(ProductsActions.PRODUCTS_DELETE)
      .switchMap((action: ProductsActions.ProductsDelete) => {
        let url = `http://ac-api.dev/products/${action.payload}`;
        return this.httpClient.delete(url)
            .map( response => {
                return {
                    type: ProductsActions.PRODUCTS_ONDELETE,
                    payload: action.payload
                };
            })
            // .catch(rsp => {
            //     return Observable.of({
            //         type: SharedActions.SHARED_LOGIN_ERROR,
            //     });
            // });
    })

  constructor(private actions$: Actions,
              private httpClient: HttpClient,
              private store: Store<ProductsReducers.State>) {}
}

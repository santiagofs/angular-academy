import { Product } from './product.model';

import * as ProductsActions from './products.actions';

import * as fromApp from '../../app.reducers';

export interface FeatureState extends fromApp.AppState {
  products: State
}

export interface State {
  products: Product[];
  fetched: boolean;
}

const initialState: State = {
  products: [],
  fetched: false
};

function indexById(products:Product[], id) {
  const index = products.findIndex((e,i,a)=>{
      return e.id === id;
  });
  return index;
}

export function ProductsReducers(state = initialState, action: ProductsActions.ProductsActions) {
  switch (action.type) {
    case ProductsActions.PRODUCTS_SET:
      return {
        ...state,
        products: [...action.payload],
        fetched: true
      };

    case ProductsActions.PRODUCTS_ADD:
      return {
        ...state,
        products: [...state.products, action.payload]
      };

    case ProductsActions.PRODUCTS_UPDATE:
      let updateIndex = indexById(state.products, action.payload.id);
      if(updateIndex === -1) return state;
      let updateProducts = [...state.products];
      updateProducts[updateIndex] = action.payload;
      return {
        ...state,
        products: updateProducts
      };

    case ProductsActions.PRODUCTS_ONDELETE:
      let deleteIndex = indexById(state.products, action.payload);
      let deleteProducts = [...state.products]
      deleteProducts.splice(deleteIndex, 1)
      return {
        ...state,
        products: deleteProducts
      };

    default:
      return state;
  }
}

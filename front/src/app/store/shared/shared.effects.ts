import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';


import { HttpClient, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import * as SharedActions from './shared.actions';
import * as fromShared from './shared.reducers';

@Injectable()
export class SharedEffects {

  @Effect()
  login$ = this.actions$
    .ofType(SharedActions.SHARED_LOGIN_REQUEST)
    .switchMap((action: SharedActions.SharedLoginRequest) => {
        return this.httpClient.post('http://ac-api.dev/login', action.payload)
            .map( response => {
                return {
                    type: SharedActions.SHARED_LOGIN_SUCCESS,
                    payload: response
                };
            })
            .catch(rsp => {
                return Observable.of({
                    type: SharedActions.SHARED_LOGIN_ERROR,
                });
            });
    })
 
    constructor(private actions$: Actions,
        private httpClient: HttpClient,
        private store: Store<fromShared.State>,
        private router: Router
    ) {}
}

import { User } from './../users/user.model';

import * as SharedActions from './shared.actions';


export interface State {
  loggedUser: User,
  loginErrorMessage: string;
}

const initialState: State = {
  loggedUser: null,
  // loggedUser: {
  //   id: 1,
  //   name: 'Santiago',
  //   email: 'santiagofs@gmail.com',
  //   api_token: '473AIhMXyPrfIw81UOpqOKzDKzUZN4G5G1rc2tqkB32qfaUE6OeL18jCsMXX',
  //   role_id: 10
  // },
  loginErrorMessage: null
};

export function SharedReducers(state = initialState, action: SharedActions.SharedActions) {

  switch (action.type) {
    //case SharedActions.SHARED_LOGIN_REQUEST

    case SharedActions.SHARED_LOGIN_SUCCESS:
      return {
        ...state,
        loggedUser: action.payload,
        loginErrorMessage: null
      };

    case SharedActions.SHARED_LOGIN_ERROR:
      return {
        ...state,
        loggedUser: null,
        loginErrorMessage: 'Invalid username or password'
      };
    
    case SharedActions.SHARED_LOGOUT:
      return {
        ...state,
        loggedUser: null,
        loginErrorMessage: null
      };

    default:
      return state;
  }
}

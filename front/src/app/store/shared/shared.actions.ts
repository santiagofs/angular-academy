import { Action } from '@ngrx/store';

import { User } from '../users/user.model';

export const SHARED_LOGIN_REQUEST = 'SHARED_LOGIN_REQUEST';
export const SHARED_LOGIN_SUCCESS = 'SHARED_LOGIN_SUCCESS';
export const SHARED_LOGIN_ERROR = 'SHARED_LOGIN_ERROR';
export const SHARED_LOGOUT = 'SHARED_LOGOUT';

export class SharedLoginRequest implements Action {
    readonly type = SHARED_LOGIN_REQUEST;
    constructor(public payload:{username: string, password: string}) {}
}
  
export class SharedLoginSuccess implements Action {
    readonly type = SHARED_LOGIN_SUCCESS;
    constructor(public payload: User) {}
}

export class SharedLoginError implements Action {
    readonly type = SHARED_LOGIN_ERROR;
}

export class SharedLogout implements Action {
    readonly type = SHARED_LOGOUT;
}


export type SharedActions = SharedLoginRequest | SharedLoginSuccess | SharedLoginError | SharedLogout;
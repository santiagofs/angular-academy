import { AdminGuard, ManagerGuard, UserGuard } from './store/shared/auth-guard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { PublicComponent } from './public/public.component';
import { AdminComponent } from './admin/admin.component';

import { AppRoutingModule } from './app.routing';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoginComponent } from './login/login.component';

import { reducers } from './app.reducers';

import { ProductsEffects } from './store/products/products.effects'
import { SharedEffects } from './store/shared/shared.effects'


@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([ProductsEffects, SharedEffects]),
    StoreRouterConnectingModule,
    // !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [AdminGuard,ManagerGuard, UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

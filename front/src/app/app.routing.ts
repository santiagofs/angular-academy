import { ManagerGuard, AdminGuard } from './store/shared/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: ErrorPageComponent},
  { path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule',
    canActivate: [ManagerGuard]
  },
  { path: '', loadChildren: 'app/public/public.module#PublicModule' }
];

@NgModule({
  imports: [
    // RouterModule.forRoot(appRoutes, {useHash: true})
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EffectsModule } from '@ngrx/effects';

import { AdminRouting } from './admin.routing';
import { AdminComponent } from './admin.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UsersEditComponent } from './users/users-edit/users-edit.component';
import { ProductsListComponent } from './products/products-list/products-list.component';
import { ProductsEditComponent } from './products/products-edit/products-edit.component';
import { OrdersListComponent } from './orders/orders-list/orders-list.component';
import { OrdersReviewComponent } from './orders/orders-review/orders-review.component';
import { OrdersComponent } from './orders/orders.component';
import { ProductsComponent } from './products/products.component';
import { UsersComponent } from './users/users.component';

import { UsersEffects } from '../store/users/users.effects';

@NgModule({
  imports: [
    AdminRouting,
    CommonModule,
    FormsModule,
    EffectsModule.forRoot([UsersEffects]),
  ],
  declarations: [
    AdminComponent,
    UsersListComponent,
    UsersEditComponent,
    ProductsListComponent,
    ProductsEditComponent,
    OrdersListComponent,
    OrdersReviewComponent,
    OrdersComponent,
    ProductsComponent,
    UsersComponent,
  ]
})
export class AdminModule { }

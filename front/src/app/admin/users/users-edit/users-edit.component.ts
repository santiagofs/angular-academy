import { NgForm } from '@angular/forms';
import { User } from './../../../store/users/user.model';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import * as fromApp from '../../../app.reducers';
import * as fromUsers from '../../../store/users/users.reducers';
import * as UsersActions from '../../../store/users/users.actions';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {

  user: User;
  id: number;
  title = '';

  constructor(private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromApp.AppState>,
    private actions$: Actions) {}

    ngOnInit() {
      this.route.params
        .subscribe(
          (params: Params) => {
            this.id = +params['id'];
            if (this.id === 0) {
              this.user = new User();
              this.user.role_id = 1;
              this.title = 'New User';
            } else {
              this.store.select('users').subscribe((state: fromUsers.State) => {

                const filtered = state.users.filter((e, i, a) => {
                  return e.id === this.id;
                });
                if (filtered.length === 0 && state.fetched) {
                  this.router.navigate(['not-found']);
                }
                this.user = {...filtered[0]};
                this.title = 'Edit user ' + this.user.name;
              });
            }

          }
        );

      this.actions$
        .ofType(UsersActions.USERS_ADD)
        .do(() => {
          this.router.navigate(['/admin/users/list']);
        }).subscribe();

      this.actions$
        .ofType(UsersActions.USERS_UPDATE)
        .do(() => {
          this.router.navigate(['/admin/users/list']);
        }).subscribe();
    }

    onSave(form: NgForm) {
      this.store.dispatch(new UsersActions.UsersSave(this.user));
    }

}

import { Store } from '@ngrx/store';
import { UsersFetch } from './../../store/users/users.actions';
import { Component, OnInit } from '@angular/core';

import * as fromApp from '../../app.reducers';
import * as UsersActions from '../../store/users/users.actions';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {
    this.store.dispatch(new UsersActions.UsersFetch());
  }

}

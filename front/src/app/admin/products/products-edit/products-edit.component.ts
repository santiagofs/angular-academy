import { NgForm } from '@angular/forms';
import { AppState } from './../../../app.reducers';
import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import * as fromApp from '../../../app.reducers';
import * as fromProducts from '../../../store/products/products.reducers';
import * as ProductsActions from '../../../store/products/products.actions';
import { Product } from '../../../store/products/product.model'

@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.component.html',
  styleUrls: ['./products-edit.component.css']
})
export class ProductsEditComponent implements OnInit {

  product: Product;
  id: number;
  title = '';

  

  constructor(private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromApp.AppState>,
    private actions$: Actions) {}

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          if(this.id === 0) {
            this.product = new Product();
            this.title = 'New Product';
          } else {
            this.store.select('products').subscribe((state:fromProducts.State) => {
              let filtered = state.products.filter((e,i,a) => {
                return e.id === this.id;
              })
              if(filtered.length === 0) {
                this.router.navigate(['not-found']);
              }
              this.product = {...filtered[0]};
              this.title = 'Edit product ' + this.product.name;
            });
          }

        }
      );

    this.actions$
      .ofType(ProductsActions.PRODUCTS_ADD)
      .do(() => {
        this.router.navigate(['/admin/products/list']);
      }).subscribe();

    this.actions$
      .ofType(ProductsActions.PRODUCTS_UPDATE)
      .do(() => {
        this.router.navigate(['/admin/products/list']);
      }).subscribe();
  }
  
  onSave(form:NgForm) {
    this.store.dispatch(new ProductsActions.ProductsSave(this.product));
  }

}

import { ProductsDelete } from './../../../store/products/products.actions';
import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as ProductsActions from '../../../store/products/products.actions';
import * as fromProducts from '../../../store/products/products.reducers';
import * as fromApp from '../../../app.reducers'

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  productsState: Observable<fromProducts.State>;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private store: Store<fromApp.AppState>) {}
  
    ngOnInit() {
      this.productsState = this.store.select('products');
    }
    
    onRemove(id) {
      this.store.dispatch(new ProductsActions.ProductsDelete(id));
    }

}